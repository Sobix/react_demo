var io = require('../lib/socket.io.js');
var Emitter = require('./emitter.js');
var config = require('./config.js');

var ServerClass = (function(){

    var sockmsg = [],
        secure = (location.protocol == 'https:'),
        subscriptionList = [
            {"name": "request"},
            {"name": "response"},
            {"name": "connect"},
            {"name": "data:push"},
            {"name": "disconnect"}
        ];

    function processServData(data, server) {


        if (typeof data.type === 'undefined') {
            return false;
        }
        if (data.data === '[{undefined]') {
            console.error("Push from server with error data: " + data["data"]);
            return false;
        }

        NAX[server].subscribe.emit("data:push", {
            data: data.data,
            type: data.type
        });
    }

    function checkMessageById(id) {
        for (var k in sockmsg) {
            if (sockmsg[k].msgid == id) {
                return k;
            }
        }
        return -1;
    }

    function generateMessageId() {
        var id = Date.now();
        while (checkMessageById(id) !== -1)
            id++;
        return id;
    }

       return {

            config: {
                sslhost:    config.servers.server.sslhost,
                host:       config.servers.server.host
            },

            subscribe: new Emitter(subscriptionList),

            /**
             *
             * @deprecated
             * @param msgid {Number} ������������� ���������
             * @returns {Object|null}
             */

            getMessageById: function (msgid) {
                for (var k in sockmsg) {
                    if (sockmsg[k].msgid == msgid) {
                        return sockmsg[k];
                    }
                }
                return null;
            },


            connect: function () {
                var self = this;
                if (typeof io === 'undefined') {
                    throw console.error('Socket.IO is not defined');
                }
                socket = io.connect(secure ? self.config.sslhost : self.config.host, {
                    'connect timeout': 500,
                    'reconnect': true,
                    'reconnection delay': 500,
                    'reopen delay': 500,
                    'max reconnection attempts': 10,
                    'secure': secure
                });
                //        }
                socket.on('a', function (a) {
                    if (a.msgid == undefined) {
                        return processServData(a, self.config.erverName);
                    }
                    var id = checkMessageById(parseInt(a.msgid));
                    if (id != -1) {
                        self.subscribe.emit("response", a);
                        if (sockmsg[id].callback) {
                            //sockmsg[id].callback( msgpack.decode( a.data ) );
                            sockmsg[id].callback(a.data);
                        }
                        if (sockmsg[id].exp > 0) {
                            localStorage.setItem('nax_server_cache_' + sockmsg[id].type + '_' +
                            JSON.stringify(sockmsg[id].data), JSON.stringify({
                                exp: sockmsg[id].exp,
                                data: JSON.parse(a.data)
                            }));
                        } else if (sockmsg[id].ttl > 0) {
                            localStorage.setItem('nax_server_cache_' + sockmsg[id].type + '_' +
                            JSON.stringify(sockmsg[id].data), JSON.stringify({
                                exp: Date.now() + (sockmsg[id].ttl * 1000),
                                data: JSON.parse(a.data)
                            }));
                        }
                        delete sockmsg[id];
                        return false;
                    }
                });
                socket.on('connect', function () {
                    self.subscribe.emit("connect");
                    //console.log('ok');
                });
                socket.on('disconnect', function () {
                    self.subscribe.emit("disconnect");
                    //console.log('ok');
                });
                return true;
            },
            disconnect: function () {
                var self = this;
                socket.disconnect();
                self.subscribe.emit("disconnect");
            },
            /**
             * ����� �������� ������� �� ������, ������������ ������� ����������� ������� �� �������.
             * ���� � [localStorage] ���� �������������� ����� � ��� ����� ����������
             * ������ �������� ������� [exp], ����� ������ ������� �������� �������������� ������
             *
             * @param {string} type - ������ ������ � node.js �� ������� �������
             * @param {object} data -
             * @param {function} callback - ������� ��������� ������ �� �������
             * @param {number} ttl - ������ ��� ��������� ������������� ������ (sec)
             * @param {number} exp - ����� ���������� �������� ������������� ������ (timestamp)
             *
             * @callback callback
             */
            send: function (type, data, callback, ttl, exp) {
                data = data || {};
                ttl = ttl || 0;
                exp = exp || 0;
                callback = callback || false;
                var now = Date.now(),
                    self = this;
                var msg = {
                    type: type,
                    msgid: generateMessageId(),
                    data: data
                };
                console.log(msg);
                if (socket != undefined) {
                    var cached = localStorage.getItem('nax_server_cache_' + type + '_' + JSON.stringify(data));
                    if (cached == undefined) {
                        socket.emit('d', msg);
                        msg.callback = callback;
                        msg.ttl = ttl;
                        msg.exp = exp;
                        sockmsg.push(msg);
                        this.subscribe.emit("request", msg);
                    } else {
                        cached = JSON.parse(cached);
                        if (cached['exp'] > now) {
                            this.subscribe.emit("request", msg);
                            msg['data'] = cached['data'];
                            this.subscribe.emit("response", msg);
                            if (callback) {
                                callback(msg['data']);
                            }
                        } else {
                            socket.emit('d', msg);
                            msg.callback = callback;
                            sockmsg.push(msg);
                            this.subscribe.emit("request", msg);
                        }
                    }
                }
            }
        };
})();

module.exports = ServerClass;
