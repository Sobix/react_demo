/**
 * ����� ������������ �������� ������ "��������" ��� ����������
 * ������� ����� ������� ��������
 *
 * @param {Array} events - ����� ������������
 * @returns {{on: Function, off: Function, emit: Function}}
 * @constructor
 */

var EmmiterClass = function( events ) {
  events = events || [];
  var subscriptions = {};
  for ( var i = 0; i < events.length; i++ ) {
    var item = events[ i ];
    subscriptions[ item[ "name" ] ] = {
      contacts : [],
      gather   : item[ "gather" ] || false
    };
    if ( item[ "trigger" ] != undefined ) {
      subscriptions[ item[ "name" ] ][ "trigger" ] = item[ "trigger" ];
    }
    if ( item[ "sub" ] != undefined ) {
      subscriptions[ item[ "name" ] ][ "sub" ] = item[ "sub" ];
    }
    if ( item[ "unsub" ] != undefined ) {
      subscriptions[ item[ "name" ] ][ "unsub" ] = item[ "unsub" ];
    }
  }
  function checkSubscribe( name ) {
    if ( subscriptions[ name ] == undefined ) {
      console.warn( "Try use undefined subscription - " + name );
      return true;
    }
    return false
  }

  function checkContacts( name ) {
    return subscriptions[ name ].contacts.length === 0;
  }

  return {
    on   : function( name, func ) {
      /*var name = arguments[ 0 ];
       var func = arguments[ 1 ];
       var data = null;
       if ( arguments.length == 3 ) {
       data = arguments[ 1 ];
       func = arguments[ 2 ];
       }*/
      if ( checkSubscribe( name ) ) {
        return;
      }
      var isCallback = subscriptions[ name ][ "sub" ] || null;
      if ( isCallback && subscriptions[ name ][ "sub" ][ "header" ] != undefined ) {
        subscriptions[ name ][ "sub" ][ "header" ]();
      }
      subscriptions[ name ].contacts.push( func );
      return subscriptions[ name ].contacts.length - 1
    },
    off  : function( name, index ) {
      if ( checkSubscribe( name ) ) {
        return;
      }
      var isCallback = subscriptions[ name ][ "unsub" ] || null;
      if ( isCallback && subscriptions[ name ][ "unsub" ][ "header" ] != undefined ) {
        subscriptions[ name ][ "sub" ][ "header" ]();
      }
      if ( typeof subscriptions[ name ].contacts[ index ] !== 'undefined' ) {
        subscriptions[ name ].contacts[ index ] = undefined;
      }

    },
    emit : function( name, args ) {
      if ( checkSubscribe( name ) ) {
        return;
      }
      if ( checkContacts( name ) ) {
        return;
      }

      var isCallback = subscriptions[ name ][ "trigger" ] || null;
      if ( isCallback && subscriptions[ name ][ "trigger" ][ "header" ] != undefined ) {
        subscriptions[ name ][ "trigger" ][ "header" ]();
      }

      if ( subscriptions[ name ][ "gather" ] ) {
        var ret = false;
        for ( var n in subscriptions[ name ].contacts ) {
          if ( subscriptions[ name ].contacts[ n ] != undefined ) {
            ret = ret || subscriptions[ name ].contacts[ n ]( args );
          }
        }
        return ret;
      } else {
        for ( var n in subscriptions[ name ].contacts ) {
          if ( subscriptions[ name ].contacts[ n ] != undefined ) {
            if ( isCallback && subscriptions[ name ][ "trigger" ][ "body" ] != undefined ) {
              subscriptions[ name ][ "trigger" ][ "body" ]();
            }
            if ( isCallback && subscriptions[ name ][ "trigger" ][ "args" ] != undefined ) {
              args = subscriptions[ name ][ "trigger" ][ "args" ];
            }
            if ( typeof subscriptions[ name ].contacts[ n ] == "function" ) {
              subscriptions[ name ].contacts[ n ]( args );
            }
          }
        }
      }

    }
  }
};

module.exports = EmmiterClass;