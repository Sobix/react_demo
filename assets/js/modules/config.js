var config = {
    servers : {
        local_server: {
            host   : 'http://192.168.3.105:2224',
            sslhost : 'https://192.168.3.105:2224'
        },
        server:{
            host   : 'http://52.30.130.104:8090',
            sslhost : 'https://52.30.130.104:8092'
        },
        //server:{
        //    host   : 'http://dataserver.finbet.kz:8090',
        //    sslhost : 'https://dataserver.finbet.kz:8091'
        //},
        server_test: {
            host   : 'http://52.16.170.8:8080',
            sslhost : 'https://52.30.130.104:8083'
        }
    },

	cookie_domain  : ".finbet.com",
    video_host     : '//52.18.112.212'
};

module.exports = config;
