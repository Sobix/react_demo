'use strict';

var NAX = (function(){
    var React = require('react');
    var Emitter = require('./modules/emitter.js');
    var config = require('./modules/config.js');
    var ClickCounter = require('./clickCounter.jsx');

    React.render(
        <ClickCounter/>,
        document.getElementById('main')
    );

    return {
        server: require('./modules/server.js')
    }
})();

