var tpl = "60seconds",//template for builder
	path = "../assets",
    coreLib = "../nax_core/lib",
    coreBase = "../nax_core/general",
    dir = "/srv/cdn";

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

	    mince: {
		    main :{
			    options: {
				    include: [path+'/js/lib', path+'/js/modules']
			    },
			    files: [{
				    src:    path+'/js/main.js',
				    dest:   path+'/js/bundle.js'
			    }]
		    }
	    },

        concat: {
            dist: {
                src: [
                    coreLib+'/jquery/jquery-1.11.2.min.js',
                    coreBase+'/loader.js',
                    coreLib+'/jquery/jquery-ui.min.js',
                    coreLib+'/datetimepicker/jquery-ui-timepicker.js',
                    coreLib+'/socket.io.js',

                    coreLib+'/chart/highcharts.js',
                    coreLib+'/chart/highcharts-more.js',
                    coreLib+'/chart/solid-gauge.js',
                    coreLib+'/notify.min.js',
                    coreLib+'/jquery.cookie.js',

                    coreBase+'/helpers.js',
                    coreBase+'/emitter.js',
                    coreBase+'/helpers-ui.js',
                    coreBase+'/config_bild.js',

                    coreBase+'/nax.js',

                    coreBase+'/router.js',
                    coreBase+'/server.js',
                    coreBase+'/data.js',

                    coreBase+'/market.js',
                    coreBase+'/personal.js',
                    coreBase+'/trade.js',
                    coreBase+'/sounds.js',
                    coreBase+'/chart.js',
                    coreBase+'/animation.js'],

                dest: path+'/js/bundle.js'
            }
        },

        uglify: {
            options: {
                stripBanners: true,
                banner: '/* <%=pkg.name %> <%=pkg.version %> <%=grunt.template.today("yyyy-mm-dd") %>*/\n'
            },
            build: {
                src:    path+'/js/bundle.js',
                dest:   dir+'/js/lite_bundle.min.js'
            }
        },

	    sass: {
		    options: {

		    },
		    dist: {
			    files: [{
				    src:    path+'/css/main.scss',
				    dest:   path+'/css/bundle.css'
			    }]
		    }
	    },

        cssmin: {
            options: {
                stripBanners: true,
                baner: '/* Minified CSS */'
            },
            build: {
                src:[path+'/css/bundle.css'],
                dest: dir+'/css/lite_bundle.min.css'
            }
        },

        watch: {
            files: [path+'/js/*.js', path+'/js/*/*.js', path+'/css/*.scss', path+'/css/*/*.scss'],
            tasks: ['mince','sass']
        }
    });


	grunt.loadNpmTasks('grunt-mincer');
    grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['mince', 'sass', 'uglify', 'cssmin', 'watch']);

    grunt.registerTask('deploy', ['concat', 'uglify', 'cssmin']);

};